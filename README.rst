.. image:: https://gitlab.com/pyFBS/pyFBS/-/raw/master/docs/logo/logo-big.png
	:align: right
	:width: 300

pyFBS
-----
	
pyFBS is a Python package for Frequency Based Substructuring, Transfer Path Analysis and also, as a new addition, multi-reference modal identification. 
It enables the user to use state-of-the-art dynamic substructuring methodologies in an intuitive manner. 

With the package also basic and application examples are provided, together with real datasets so you can directly try out the capabilities of the pyFBS.

|pypi| |docs| |codequality| |MIT|

Features
--------

* 3D display

* FRF synthetization

* Virtual Point Transformation

* System Equivalent Model Mixing

* Singular Vector Transformation

* Experimental Modal Analysis

For more information on features, basic and application examples check out the `documentation`_. 

Citation
--------
A paper about the pyFBS will be submitted to the Journal of Open Source Software journal. If you will be using pyFBS in your scientific research, please consider citing the paper.

License
-------
Licensed under the MIT license.

.. _documentation: https://pyfbs.readthedocs.io/en/latest/intro.html

.. |pypi| image:: https://img.shields.io/pypi/v/pyfbs?style=flat-square
   :target: https://pypi.org/project/pyfbs/

.. |docs| image:: https://readthedocs.org/projects/pyfbs/badge/?version=latest
   :target: https://pyfbs.readthedocs.io/en/latest/?badge=latest

.. |MIT| image:: https://img.shields.io/badge/License-MIT-yellow.svg
   :target: https://opensource.org/licenses/MIT
   
.. |codecov| image:: https://codecov.io/gl/pyFBS/pyFBS/branch/\x6d6173746572/graph/badge.svg?token=XSGM89JGMF
   :target: https://codecov.io/gl/pyFBS/pyFBS

.. |codequality| image:: https://app.codacy.com/project/badge/Grade/dbb59e10c07543b6b61c083a09eac500    
   :target: https://www.codacy.com/gl/pyFBS/pyFBS/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=pyFBS/pyFBS&amp;utm_campaign=Badge_Grade