***************
Getting started
***************

.. toctree::
    :hidden:
 
    ./credits.rst 
    ./license.rst 
    ./contribute.rst
    ./installation.rst 
    ./usage.rst 

.. panels::
    :column: col-lg-6 col-md-6 col-sm-12 col-xs-12 p-3

    Credits
    ^^^^^^^^^^^^

    See the authors of the pyFBS project.

    .. link-button:: credits
        :type: ref
        :text: Credits
        :classes: btn-outline-primary btn-block stretched-link
    
    ---
    License
    ^^^^^^^^^^^^

    The project is under the MIT license.

    .. link-button:: license
        :type: ref
        :text: License
        :classes: btn-outline-primary btn-block stretched-link
    
    ---
    Contribute
    ^^^^^^^^^^^^

    Everyone can contribute to the pyFBS library, take a look at how can you do that.

    .. link-button:: contribute
        :type: ref
        :text: Contribute
        :classes: btn-outline-primary btn-block stretched-link
 
    ---
    Installation
    ^^^^^^^^^^^^

    Installation process for Python version 3.8+.

    .. link-button:: installation
        :type: ref
        :text: Installation
        :classes: btn-outline-primary btn-block stretched-link
    ---
    Usage
    ^^^^^^^^^^^^

    Get familiar with the example data and features that pyFBS offers.

    .. link-button:: usage
        :type: ref
        :text: Usage
        :classes: btn-outline-primary btn-block stretched-link
