====================
Component-based TPA
====================
For an independent characterization of the source structure, component-based TPA adopts a set of equivalent forces applied at the interface between active and passive side. 
These equivalent or blocked forces are valid for any assembly with a modified passive side.

   
.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="in-situ TPA">

.. only:: html

    .. figure:: ./../data/tpa.png   
       :target: 11_insitu_TPA.html

       in-situ TPA

.. raw:: html

    </div>

.. toctree::
   :hidden:

   11_insitu_TPA





.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Free velocity">

.. only:: html

    .. figure:: ./../data/free_velocity.PNG   
       :target: 16_free_velocity.html

       Free velocity

.. raw:: html

    </div>

.. toctree::
   :hidden:

   16_free_velocity




.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Pseudo-forces">

.. only:: html

    .. figure:: ./../data/pseudo_forces.PNG   
       :target: 17_pseudo_forces.html

       Pseudo-forces

.. raw:: html

    </div>

.. toctree::
   :hidden:

   17_pseudo_forces