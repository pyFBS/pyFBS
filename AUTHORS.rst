Development Lead
----------------

* Tomaž Bregar 

* Ahmed El Mahmoudi

* Miha Kodrič

* Domen Ocepek

* Francesco Trainotti

* Miha Pogačar

* Mert Göldeli

Contributors
------------

* Gregor Čepon

* Miha Boltežar

* Daniel J. Rixen

For a full list of `contributors`_ check the repository.

.. _contributors: https://gitlab.com/pyFBS/pyFBS/-/graphs/master
